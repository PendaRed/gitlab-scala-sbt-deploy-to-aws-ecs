import sbt.Keys.libraryDependencies

ThisBuild / scalaVersion := "2.13.7"
ThisBuild / organization := "com.jgibbons"

lazy val commonSettings = Seq(
  assembly / test := {},
  assembly / assemblyMergeStrategy := {
    case "logback.xml" => MergeStrategy.last
    case x if x.endsWith("io.netty.versions.properties") => MergeStrategy.last
    case PathList("org", "slf4j", xs@_*) => MergeStrategy.last
    case x if x.contains("swagger-ui") && x.endsWith("pom.properties") =>
      // Tapir reads its own pom to get its version number, so it needs to be included in the fat jar
      MergeStrategy.first
    case x => (assembly / assemblyMergeStrategy).value(x)
  },
)

lazy val service1 = (project in file("service1"))
  .settings(commonSettings: _*)
  .settings(
    name := "service1",
    assembly / mainClass := Some("com.jgibbons.svc1.MainService1"),
    assembly / assemblyOutputPath := file("docker/svc1.jar"),
    libraryDependencies ++= Dependencies.awsDependencies
  )

lazy val demoEcs = (project in file("."))
  .aggregate(service1)
  
