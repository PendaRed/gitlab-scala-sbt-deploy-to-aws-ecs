package com.jgibbons.svc1

import cats.effect.IO

case class ApiHandlers() {
  def countCharacters(s: String): IO[Either[Unit, String]] =
    IO {Right[Unit, String](""+s.length)}
}
