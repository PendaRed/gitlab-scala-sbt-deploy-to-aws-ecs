package com.jgibbons.svc1

import com.typesafe.config.{Config, ConfigFactory}

case class ServerCfg(hostName: String, portNum: Int)

case class MicroServiceConfig(cfg: Config) {
  val serverConfig: ServerCfg = createServerConfig(cfg.getConfig("server"))

  private def createServerConfig(c: Config): ServerCfg =
    ServerCfg(c.getString("host"), c.getInt("port"))

}

object MicroServiceConfig {
  def apply(): ServerCfg = {
    val cfg = ConfigFactory.load()
    val c = MicroServiceConfig(cfg)
    c.serverConfig
  }
}

