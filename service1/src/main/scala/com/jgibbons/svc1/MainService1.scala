package com.jgibbons.svc1

import cats.data.Kleisli
import cats.effect.{ExitCode, IO, IOApp}
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import org.http4s.{Request, Response}

object MainService1 extends IOApp {

  def makeRouter(): Kleisli[IO, Request[IO], Response[IO]] = {
    val apiEndpoints = new ApiEndpoints(ApiHandlers())
    val routes = apiEndpoints.makeRoutes()

    Router[IO](
      "" -> routes
    ).orNotFound
  }

  private def serveStream(serverConfig: ServerCfg) = {
    for {
      server <- BlazeServerBuilder[IO]
        .bindHttp(serverConfig.portNum, serverConfig.hostName)
        .withHttpApp(makeRouter())
        .resource
    } yield server
  }

  override def run(args: List[String]): IO[ExitCode] = {
    val serverCfg = MicroServiceConfig()
    serveStream(serverCfg).use(_ => {
      println(s"Go to: http://${serverCfg.hostName}:${serverCfg.portNum}/docs")
      IO.never
    }).as(ExitCode.Success)
  }
}