# Example to deploy an SBT project from GitLab to AWS Fargate

This code matches the notes at:

http://gibbons.org.uk/gitlab-scala-deploy-to-ecs-apr-2022

## AWS notes
From the cluster->service->task
Click on task.  In the Network section you will see ENI Id, click on it

This gets you to the Network interfaces.  Scroll right and click on Security Groups

If you set up the fargate service with forceNewDeployment, then when the latest tagged
image updates the service should restart.

### create the fat jar
```
sbt clean assembly
```

### dockerise it

```
cd docker
docker build -t svc1 .

// -t pseudo tty
// -d detached
// -i Keep STDIN open even if not attached
// -p port publish, host:container
//docker run -it -dp 80:8080 svc1
docker run -dp 80:8080 svc1
```

### Publish it to Amazon Container Registry

https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html

A registry is a private registry
A repository holds an image - ie usually you have an image in each repository.
  - you can use namespace, so the tag could be :myteam/latest.  allowing each team
    their own version of the image

How to pass pseudo tty
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html

```
eg for accountId: 111111111111
// My ecr user : arn:aws:iam::111111111111:user/ecr_user
// aws configure --profile ecr_user
// then paste in the access keys, can also import from a csv, as here
// https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html
// BUT actually do it like this;
// https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html

aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 111111111111.dkr.ecr.eu-west-1.amazonaws.com

docker tag svc1:latest 111111111111.dkr.ecr.eu-west-1.amazonaws.com/svc1:latest


docker push 111111111111.dkr.ecr.eu-west-1.amazonaws.com/svc1:latest
```

## Docker useful commands

https://docs.docker.com/engine/reference/run/

```
// to remove a tag and leave the image
docker rmi tagName

// get the names of even stopped containers
docker ps --all

docker container logs <name>

docker images
docker image rm <Image_id> --force

docker images --filter reference=sv1

docker images
-- you can run it interactive with a shell to look
docker run -it --entrypoint sh svc1 
-- run it detached with port mapping
docker run -it -dp 80:80 svc1
```
