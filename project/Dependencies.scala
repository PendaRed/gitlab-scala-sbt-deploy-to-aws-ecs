import sbt._

object Dependencies {
  private val ScalaTestVersion = "3.2.9"
  private val ScalaLoggingVersion = "3.9.4"
  private val LogbackVersion = "1.2.3"
  private val CirceVersion="0.15.0-M1"
  private val AwsSdkVersion="2.17.157"
  private val Fs2Version = "3.2.4"
  private val Http4sVersion = "0.23.7"
  private val TapirSwaggerVersion = "0.20.0-M6"
  private val TypesafeConfigVersion = "1.4.0"

  val ConfigDependencies: Seq[ModuleID] = Seq(
    "com.typesafe" % "config" % TypesafeConfigVersion)

  val commonSettingsDependencies = Seq(
    "org.scalatest" % "scalatest_3" % ScalaTestVersion % Test,
  ) ++ ConfigDependencies

  val circeDepenencies = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % CirceVersion)

  val ServiceDependencies = Seq(
    "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
    "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
    "org.http4s"      %% "http4s-circe"        % Http4sVersion,
    "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % TapirSwaggerVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % TapirSwaggerVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % TapirSwaggerVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-cats" % TapirSwaggerVersion
  )

  val sharedDependencies = Seq(

  ) ++ commonSettingsDependencies

  val awsDependencies = Seq(
    "com.typesafe.scala-logging" % "scala-logging_3" % ScalaLoggingVersion,
    "ch.qos.logback" % "logback-classic" % LogbackVersion,
    "software.amazon.awssdk" % "ec2" % AwsSdkVersion,
    "software.amazon.awssdk" % "ecs" % AwsSdkVersion,
  ) ++ commonSettingsDependencies ++ circeDepenencies ++ ServiceDependencies

}